import React, { useState, useEffect } from "react";
import {
  Box,
  Card,
  CardActions,
  CardContent,
  Typography,
  Stack,
  CardMedia,
} from "@mui/material";
import { RiGroupLine } from "react-icons/ri";
import { BsGear } from "react-icons/bs";
import { AiOutlineCalendar } from "react-icons/ai";
import Car from "../images/car.png";
import { Navigate, useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import Button from "./Button";
import { useSelector, useDispatch } from "react-redux";
import {
  removeDetailCar,
  getDetailCar,
  fetchAsyncCarsDetail,
  getBtn,
  setButton,
} from "../redux/carSlice";

const CarCardDetail = (props) => {
  const { id } = props;
  const dispatch = useDispatch();
  const data = useSelector(getDetailCar);
  const btn = useSelector(getBtn);

  console.log(data);

  useEffect(() => {
    dispatch(
      fetchAsyncCarsDetail(id),
      dispatch(setButton("Lanjutkan Pembayaran"))
    );
    return () => {
      dispatch(removeDetailCar());
    };
  }, [dispatch, id]);

  const navigate = useNavigate();

  return (
    <>
      <Box>
        {Object.keys(data).length === 0 ? (
          <Typography>...Loading</Typography>
        ) : (
          <Card sx={{ maxWidth: 340 }}>
            <CardMedia
              component="img"
              height="225"
              image={data.image}
              alt="car image"
            />
            <CardContent>
              <Typography
                gutterBottom
                variant="h6"
                fontWeight="bold"
                component="div"
              >
                {data.name}
              </Typography>
              <Stack
                direction={{ xs: "column", sm: "column", md: "row" }}
                spacing={2}
              >
                <Typography variant="caption" sx={{}}>
                  <RiGroupLine /> {data.passengers} Orang
                </Typography>
                <Typography variant="caption" sx={{ mt: 2 }}>
                  <BsGear /> {data.fuel}
                </Typography>
                <Typography variant="caption" sx={{ mt: 2 }}>
                  <AiOutlineCalendar /> Tahun {data.time}
                </Typography>
              </Stack>
              <Stack
                direction={{ xs: "column", sm: "column", md: "row" }}
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  flexGrow: "1",
                  fontFamily: "Helvetica Neue",
                  mt: 5,
                }}
              >
                <Typography
                  gutterBottom
                  variant="title"
                  fontWeight="normal"
                  component="div"
                >
                  Total
                </Typography>
                <Typography
                  gutterBottom
                  variant="h6"
                  fontWeight="bold"
                  component="div"
                >
                  {new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                  }).format(data.price)}{" "}
                  / hari
                </Typography>
              </Stack>
            </CardContent>
            <CardActions>
              <Button fullWidth onClick={() => navigate(`/payment/${data.id}`)}>
                {btn}
              </Button>
            </CardActions>
          </Card>
        )}
      </Box>
    </>
  );
};

export default CarCardDetail;
