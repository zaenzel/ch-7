import React from "react";
import { Stack } from "@mui/material";
import SideBar from "../componentAdmin/SideBar";
import { useParams } from "react-router-dom";
import FormUpdate from "../componentAdmin/FormUpdate";

const Update = () => {
  const { id } = useParams();
  return (
    <>
      <Stack direction="row" justifyContent="space-between">
        <SideBar />
        <FormUpdate id={id}/>
      </Stack>
    </>
  );
};

export default Update;
