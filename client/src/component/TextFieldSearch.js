import React, { useState } from "react";
import { Grid, TextField, Typography } from "@mui/material";
import { fetchAsyncCars } from "../redux/carSlice";
import { useDispatch } from "react-redux";

import Button from "./Button";

const TextFieldSearch = (props) => {
  // console.log(props.props);
  const [dataFilter, setDataFlter] = useState({ mobil: "" });
  const inputHandler = (e) => {
    setDataFlter({ ...dataFilter, [e.target.name]: e.target.value });
    console.log(dataFilter);
  };

  const dispatch = useDispatch();
  const cari = () => {
    dispatch(fetchAsyncCars(dataFilter));
  };

  return (
    <>
      <Grid
        container
        spacing={3}
        sx={{
          display: "flex",
          justifyContent: "center",
          p: 3,
        }}
      >
        <Grid
          item
          direction="column"
          sx={{
            display: "flex",
          }}
        >
          <Typography sx={{ mb: 2 }} variant="body">
            Tipe Mobil
          </Typography>
          <TextField
            id="mobil"
            name="mobil"
            variant="outlined"
            onChange={(e) => inputHandler(e)}
        
          />
        </Grid>
        <Grid
          item
          direction="column"
          sx={{
            display: "flex",
          }}
        >
          <Typography sx={{ mb: 2 }} variant="body">
            Pilih Tanggal
          </Typography>
          <TextField variant="outlined" />
        </Grid>
        <Grid
          item
          direction="column"
          sx={{
            display: "flex",
          }}
        >
          <Typography sx={{ mb: 2 }} variant="body">
            waktu jemput
          </Typography>
          <TextField
            
            id="mobil"
            // onChange={(e) => setMobil(e.target.value)}
          />
        </Grid>
        <Grid
          item
          direction="column"
          sx={{
            display: "flex",
          }}
        >
          <Typography sx={{ mb: 2 }} variant="body">
            jumlah penumpang
          </Typography>
          <TextField
            name="kapasitas"
            id="kapasitas"
            onChange={(e) => inputHandler(e)}
            
          />
        </Grid>
      </Grid>
      <Button
        variant="contained"
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          height: 40,
          width: 120,
          mr: 3,
        }}
        color="secondary"
        onClick={() => cari()}
      >
        cari
      </Button>
    </>
  );
};

export default TextFieldSearch;
