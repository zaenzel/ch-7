import {
  Box,
  Typography,
  Grid,
  Stack,
  Button,
  Container,
  Snackbar,
} from "@mui/material";
import React, { Children, useEffect, useState } from "react";
import Navbar from "./Navbar";
import { useSelector, useDispatch } from "react-redux";
import { getAllCars } from "../redux/carSlice";
import Submenu from "./Submenu";
import AddIcon from "@mui/icons-material/Add";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { Link } from "react-router-dom";
import styled from "@emotion/styled";
import CarCard from "./CarCard";
import {
  collection,
  query,
  where,
  getDocs,
  onSnapshot,
} from "firebase/firestore";
import { db } from "../firebase";

const prop = [
  { title: ["DASHBOARD", "LIST CAR"] },
  { list: ["dashboard", "list car"] },
];

const DataList = () => {
  
  const Btn = styled(Button)(({ theme }) => ({
    backgroundColor: "white",
    color: "#AEB7E1",
    border: "1px solid #AEB7E1",
    fontWeight: "bold",
  }));

  const [data, setData] = useState([]);

  useEffect(() => {
    // REALTIME
    const unsub = onSnapshot(
      collection(db, "cars"),
      (snapshot) => {
        let list = [];
        snapshot.docs.forEach((doc) => {
          list.push({ id: doc.id, ...doc.data() });
        });
        setData(list);
      },
      (error) => {
        console.log(error);
      }
    );
    return () => {
      unsub();
    };
  }, []);

  return (
    <>
      <Box flex={11}>
        <Navbar />
        <Grid container>
          <Submenu title={prop[0].title[1]} list={prop[1].list[1]} />
          <Container fluid sx={{ flex: 10, backgroundColor: "#E5E5E5E5" }}>
            <Stack marginTop={3} spacing={3} mb={5}>
              <Box sx={{ display: "flex", alignItems: "center" }}>
                <Typography variant="body" fontSize="0.75rem" fontWeight="bold">
                  {prop[0].title[1]}
                </Typography>
                <ChevronRightIcon />
                <Typography variant="list">{prop[1].list[1]}</Typography>
              </Box>
              <Box display="flex" justifyContent="space-between">
                <Typography variant="title">{prop[0].title[1]}</Typography>
                <Link to="addcar">
                  <Button variant="contained">
                    <AddIcon />
                    <Typography variant="body">Add New Car</Typography>
                  </Button>
                </Link>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  gap: 2,
                }}
              >
                <Btn>All</Btn>
                <Btn>Small</Btn>
                <Btn>Medium</Btn>
                <Btn>Large</Btn>
              </Box>
              <Grid container gap={3}>
                {Object.keys(data).length === 0
                  ? null
                  : data.map((car, index) => {
                      return (
                        <>
                          <CarCard data={car} key={index} />
                        </>
                      );
                    })}
              </Grid>
            </Stack>
          </Container>
        </Grid>
      </Box>
    </>
  );
};

export default DataList;
