import React, { useEffect, useState } from "react";
import {
  Box,
  Typography,
  Grid,
  Stack,
  Button,
  TextField,
  Snackbar,
} from "@mui/material";
import Navbar from "./Navbar";
import { useSelector, useDispatch } from "react-redux";
import { getAllCars } from "../redux/carSlice";
import Submenu from "./Submenu";
import AddIcon from "@mui/icons-material/Add";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { Link, useNavigate } from "react-router-dom";
import styled from "@emotion/styled";
import { Container } from "@mui/system";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import { doc, addDoc, collection, getDoc } from "firebase/firestore";
import { db, storage } from "../firebase";
import { ref, getDownloadURL, uploadBytesResumable } from "firebase/storage";

const Form = styled(Grid)({
  backgroundColor: "white",
});

const prop = [
  { title: ["DASHBOARD", "LIST CAR"] },
  { list: ["dashboard", "list car"] },
];

const FormAddCar = () => {
  const navigate = useNavigate();
  const [file, setFile] = useState("");
  const [data, setData] = useState({
    name: "",
    price: "",
    update: new Date().toDateString(),
  });
  const [perc, setPerc] = useState(null);

  useEffect(() => {
    const uploadFile = () => {
      const name = new Date().getTime() + file.name;
      const storageRef = ref(storage, name);

      const uploadTask = uploadBytesResumable(storageRef, file);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log("Upload is " + progress + "% done");

          setPerc(progress);
          switch (snapshot.state) {
            case "paused":
              console.log("Upload is paused");
              break;
            case "running":
              console.log("Upload is running");
              break;
            default:
              break;
          }
        },
        (error) => {
          console.log(error);
        },
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            setData((prev) => ({ ...prev, img: downloadURL }));
          });
        }
      );
    };
    file && uploadFile();
  }, [file]);

  const handleAdd = async (e) => {
    e.preventDefault();
    try {
      const res = await addDoc(collection(db, "cars"), data);
      navigate(-1);
    } catch (error) {
      console.log(error);
    }
  };

  const handleInput = (e) => {
    setData({
      ...data,
      [e.target.id]: e.target.value,
    });
    console.log(data);
  };

  console.log(data);
  return (
    <>
      <Box flex={11}>
        <Navbar />
        <Grid container>
          <Submenu title={prop[0].title[1]} list={prop[1].list[1]} />
          <Container fluid sx={{ flex: 10, backgroundColor: "#E5E5E5E5" }}>
            <Stack
              marginTop={3}
              spacing={3}
              component="form"
              onSubmit={handleAdd}
            >
              <Box sx={{ display: "flex", alignItems: "center" }}>
                <Typography variant="body" fontSize="0.75rem" fontWeight="bold">
                  {prop[0].title[1]}
                </Typography>
                <ChevronRightIcon />
                <Typography variant="body">{prop[1].list[1]}</Typography>
                <ChevronRightIcon />
                <Typography variant="list">Add New Car</Typography>
              </Box>
              <Typography variant="title">ADD NEW CAR</Typography>
              <Form container>
                <Grid Container md={6}>
                  <Grid
                    item
                    md={12}
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      maxWidth: "50%",
                      mx: 2,
                      my: 1,
                    }}
                    gap={2}
                    fluid="true"
                  >
                    <Typography>Nama</Typography>
                    <TextField
                      id="name"
                      name="name"
                      onChange={(e) => handleInput(e)}
                      variant="outlined"
                      role="dialog"
                      fluid="true"
                      fullWidth
                      required
                    />
                  </Grid>
                  <Grid
                    item
                    md={12}
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      maxWidth: "50%",
                      mx: 2,
                      my: 1,
                    }}
                    gap={2}
                  >
                    <Typography>Harga</Typography>
                    <TextField
                      id="price"
                      name="price"
                      type="number"
                      onChange={handleInput}
                      variant="outlined"
                      fullWidth
                      required
                    />
                  </Grid>
                  <Grid
                    item
                    md={12}
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      maxWidth: "50%",
                      mx: 2,
                      my: 1,
                    }}
                    gap={2}
                  >
                    <Typography>Tahun</Typography>
                    <TextField
                      id="year"
                      name="year"
                      type="number"
                      onChange={handleInput}
                      variant="outlined"
                      fullWidth
                      required
                    />
                  </Grid>
                  <Grid
                    item
                    md={12}
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      maxWidth: "50%",
                      mx: 2,
                      my: 1,
                    }}
                    gap={2}
                  >
                    <Typography>Foto</Typography>
                    {/* <TextField
                    type="file"
                    id="file"
                    name="file"
                    onChange={(e) => setFile(e.target.files[0])}
                  /> */}
                    <Button
                      onChange={(e) => setFile(e.target.files[0])}
                      variant="outlined"
                      endIcon={<FileUploadIcon />}
                    >
                      <label htmlFor="file">Image</label>
                      <input
                        type="file"
                        id="file"
                        style={{ display: "none", cursor: "pointer" }}
                      />
                    </Button>
                  </Grid>
                </Grid>
                <Grid
                  Container
                  md={6}
                  sx={{ display: "flex", justifyContent: "center" }}
                >
                  <Box mt={3}>
                    <img
                      src={
                        file
                          ? URL.createObjectURL(file)
                          : "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg"
                      }
                      width="200px"
                      height="200px"
                      alt="img"
                    />
                  </Box>
                </Grid>
              </Form>
              <Box sx={{ display: "flex" }} gap={3}>
                <Button
                  variant="outlined"
                  sx={{
                    maxWidth: "100px",
                  }}
                  onClick={(e) => navigate(-1)}
                >
                  Cancel
                </Button>
                <Button
                  variant="contained"
                  sx={{
                    maxWidth: "100px",
                  }}
                  type="submit"
                  disabled={perc !== null && perc < 100}
                >
                  Save
                </Button>
              </Box>
            </Stack>
          </Container>
        </Grid>
      </Box>
    </>
  );
};

export default FormAddCar;
