import {
  Box,
  Container,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  Grid,
  Stack,
  TextField,
} from "@mui/material";
import React, { useState } from "react";
import Navbar from "./Navbar";
import Submenu from "./Submenu";
import { FcSurvey } from "react-icons/fc";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import Chart from "./Chart";


const columns = [
  { id: "name", label: "Name", minWidth: 170 },
  { id: "code", label: "ISO\u00a0Code", minWidth: 100 },
  {
    id: "population",
    label: "Population",
    minWidth: 170,
    align: "right",
    format: (value) => value.toLocaleString("en-US"),
  },
  {
    id: "size",
    label: "Size\u00a0(km\u00b2)",
    minWidth: 170,
    align: "right",
    format: (value) => value.toLocaleString("en-US"),
  },
  {
    id: "density",
    label: "Density",
    minWidth: 170,
    align: "right",
    format: (value) => value.toFixed(2),
  },
];

function createData(name, code, population, size) {
  const density = population / size;
  return { name, code, population, size, density };
}

const rows = [
  createData("India", "IN", 1324171354, 3287263),
  createData("China", "CN", 1403500365, 9596961),
  createData("Italy", "IT", 60483973, 301340),
  createData("United States", "US", 327167434, 9833520),
  createData("Canada", "CA", 37602103, 9984670),
  createData("Australia", "AU", 25475400, 7692024),
  createData("Germany", "DE", 83019200, 357578),
  createData("Ireland", "IE", 4857000, 70273),
  createData("Mexico", "MX", 126577691, 1972550),
  createData("Japan", "JP", 126317000, 377973),
  createData("France", "FR", 67022000, 640679),
  createData("United Kingdom", "GB", 67545757, 242495),
  createData("Russia", "RU", 146793744, 17098246),
  createData("Nigeria", "NG", 200962417, 923768),
  createData("Brazil", "BR", 210147125, 8515767),
];

const DataTable = () => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const prop = [
    { title: ["DASHBOARD", "LIST CAR"] },
    { list: ["dashboard", "list car"] },
  ];

  return (
    <>
      <Box flex={11}>
        <Navbar />
        <Grid container>
          <Submenu title={prop[0].title[0]} list={prop[1].list[0]} />
          <Container fluid sx={{ flex: 10, backgroundColor: "#E5E5E5E5" }}>
            <Stack marginTop={3} spacing={3}>
              <Box sx={{ display: "flex", alignItems: "center" }}>
                <Typography variant="body" fontSize="0.75rem" fontWeight="bold">
                  {prop[0].title[0]}
                </Typography>
                <ChevronRightIcon />
                <Typography variant="list">{prop[1].list[0]}</Typography>
              </Box>
              <Typography variant="title">{prop[0].title[0]}</Typography>

              <Box sx={{bgcolor:"white", p:2, display:"flex", flexDirection:"column", gap:5}}>
                <Typography variant="title">Data Penyewaan Mobil</Typography>    
                <Chart />
              </Box>

              <Box>
                <Box marginBottom={1}>
                  <FcSurvey fontSize="large" />
                  <Typography variant="body" fontWeight="bold">
                    List Order
                  </Typography>
                </Box>
                <Paper sx={{ width: "100%", overflow: "hidden" }}>
                  <TableContainer sx={{ maxHeight: 440 }}>
                    <Table stickyHeader aria-label="sticky table">
                      <TableHead>
                        <TableRow>
                          {columns.map((column) => (
                            <TableCell
                              key={column.id}
                              align={column.align}
                              style={{ minWidth: column.minWidth }}
                            >
                              {column.label}
                            </TableCell>
                          ))}
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {rows
                          .slice(
                            page * rowsPerPage,
                            page * rowsPerPage + rowsPerPage
                          )
                          .map((row) => {
                            return (
                              <TableRow
                                hover
                                role="checkbox"
                                tabIndex={-1}
                                key={row.code}
                              >
                                {columns.map((column) => {
                                  const value = row[column.id];
                                  return (
                                    <TableCell
                                      key={column.id}
                                      align={column.align}
                                    >
                                      {column.format &&
                                      typeof value === "number"
                                        ? column.format(value)
                                        : value}
                                    </TableCell>
                                  );
                                })}
                              </TableRow>
                            );
                          })}
                      </TableBody>
                    </Table>
                  </TableContainer>
                  <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                  />
                </Paper>
              </Box>
            </Stack>
          </Container>
        </Grid>
      </Box>
    </>
  );
};

export default DataTable;
