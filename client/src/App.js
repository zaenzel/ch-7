import React, { createContext, useContext, useEffect, useState } from "react";
import "./App.css";
import { Navigate, Route, Routes } from "react-router-dom";
import Main from "./pages/Main";
import Detail from "./pages/Detail";
import Page404 from "./pages/Page404";
import Login from "./pages/Login";
import Payment from "./pages/Payment";
import Dashboard from "./pages/Dashboard";
import ListCar from "./pages/ListCar";
import { AuthContext } from "./context/AuthContext";
import AddCar from "./pages/AddCar";
import { getUser } from "./redux/userSlice";
import { useSelector } from "react-redux";
import Update from "./pages/Update";
// export const data = createContext();

function App() {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);
  const { currentUser } = useContext(AuthContext);

  const RequireAuth = ({ children }) => {
    return currentUser ? children : <Navigate to="/login" />;
  };

  useEffect(() => {
    const getUser = () => {
      fetch("http://localhost:5000/auth/login/success", {
        method: "GET",
        credentials: "include",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Credentials": true,
        },
      })
        .then((response) => {
          if (response.status === 200) return response.json();
          setLoading(false);
          console.log(loading);
          throw new Error("Authentication failed");
        })
        .then((response) => {
          setUser(response.user);
          setLoading(false);
          console.log(loading);
          console.log(response.user);
        })
        .catch((err) => {
          console.log(err);
          setLoading(false);
          console.log(loading);
        });
    };
    getUser();
  }, []);

  return (
    <div className="App">
      {/* <data.Provider value={{ user }}>
      <Routes>
        <Route path="/">
          <Route index element={<Main />} />
          <Route
            path="login"
            element={
              user ? (
                user.isAdmin ? (
                  <Navigate to="/dashboard" />
                ) : (
                  <Navigate to="/" />
                )
              ) : (
                <Login />
              )
            }
          />
          <Route path="detail/:id" element={<Detail />} />
          <Route
            path="payment/:id"
            element={user ? <Payment /> : <Navigate to="/Login" />}
          />
          <Route path="dashboard">
            <Route
              index
              element={user ? <Dashboard /> : <Navigate to="/login" />}
            />
            <Route path="listcar" element={<ListCar />} />
          </Route>
          <Route path="*" element={<Page404 />} />
        </Route>
      </Routes>
      </data.Provider> */}

      <Routes>
        <Route path="/">
          <Route index element={<Main />} />
          <Route path="login" element={<Login />} />
          <Route path="detail/:id" element={<Detail />} />
          <Route path="payment/:id" element={<Payment />} />

          <Route path="dashboard">
            <Route
              index
              element={
                <RequireAuth>
                  <Dashboard />
                </RequireAuth>
              }
            />
            <Route path="listcar" element={<ListCar />} />
            <Route
              path="listcar/addcar"
              element={<AddCar />}
            />
            <Route
              path="listcar/update/:id"
              element={<Update />}
            />
          </Route>
          <Route path="*" element={<Page404 />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
