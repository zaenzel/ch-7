import { Stack } from "@mui/material";
import React from "react";
import DataList from "../componentAdmin/DataList";
import SideBar from "../componentAdmin/SideBar";

const ListCar = () => {
  return (
    <Stack direction="row" justifyContent="space-between">
      <SideBar />
      <DataList />
    </Stack>
  );
};

export default ListCar;
