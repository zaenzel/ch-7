import styled from "@emotion/styled";
import { Typography, Box, Stack, Container } from "@mui/material";
import React from "react";

const Submenu = (props) => {
  const Wrapper = styled(Box)(({ theme }) => ({
    flex: 2,
    minHeight: "100vh",
  }));

  const Title = styled(Typography)(({theme}) => ({
      display: "block",
      margin: "25px 0",
      color: "#8A8A8A"
  }))

  return (
    <>
      <Wrapper>
        <Container>
          <Title variant="title">{props.title}</Title>
          <Typography variant="body" fontWeight={800}>{props.list}</Typography>
        </Container>
      </Wrapper>
    </>
  )
};

export default Submenu;
