import {
    createSlice,
    createAsyncThunk,
    createEntityAdapter,
  } from "@reduxjs/toolkit";

  const initialState = {
    user: {}
  };

  const userSlice = createSlice({
      name: "user",
      initialState,
      reducers:{
          addUser: (state, {payload}) => {
              state.user = payload
          }
      }
  })

  export const { addUser } = userSlice.actions;
  export const getUser = (state) => state.user.user;
  export default userSlice.reducer; 