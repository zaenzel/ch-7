import { Box, Paper, Stack, Typography } from "@mui/material";
import React from "react";

const CardTopPayment = () => {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          flexGrow: "1",
          fontFamily: "Helvetica Neue",
          transform: "translateY(-50%)",
          margin: 5,
        }}
      >
        <Paper>
          <Stack container p={4}>
            <Typography variant="title" mb={2}>
              Detail Pesananmu
            </Typography>
            <Stack
              container
              spacing={10}
              direction={{ xs: "column", sm: "column", md: "row" }}
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Stack spacing={1}>
                <Typography variant="body">Tipe Mobil</Typography>
                <Typography variant="list">Dengan Sopir</Typography>
              </Stack>
              <Stack spacing={1}>
                <Typography variant="body">Tanggal</Typography>
                <Typography variant="list">27 Mei 2022</Typography>
              </Stack>
              <Stack spacing={1}>
                <Typography variant="body">Waktu Jemput/Antar</Typography>
                <Typography variant="list">10.00 WIB</Typography>
              </Stack>
                <Typography>Jumlah Penumpang (opsional)</Typography>
            </Stack>
          </Stack>
        </Paper>
      </Box>
    </>
  );
};

export default CardTopPayment;
