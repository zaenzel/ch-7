import { Box } from "@mui/material";
import React from "react";
import { useParams } from "react-router-dom";
import CardTopPayment from "../component/CardTopPayment";
import Footer from "../component/Footer";
import HeaderDetail from "../component/HeaderDetail";
import WraperPayment from "../component/WraperPayment";

const Payment = () => {
  return (
    <>
      <HeaderDetail />
      <Box>
        {/* <CardTopPayment /> */}
        <WraperPayment />
      </Box>
      <Footer />
    </>
  );
};

export default Payment;
