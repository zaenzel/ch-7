import React, { useEffect } from "react";
import CarCard from "../component/CarListing";
import Footer from "../component/Footer";
import Header from "../component/Header";
import Search from "../component/Search";
import { fetchAsyncCars } from "../redux/carSlice";
import { useSelector, useDispatch } from "react-redux";
import api from "../api/api";
import CarListing from "../component/CarListing";
// import { useDispatch, useSelector } from "react-redux";
// import { fetchAsyncCars } from "../redux/carSlice";

const Main = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchAsyncCars()) 
  }, [dispatch])

  return (
    <>
      <Header />
      <Search />
      <CarListing />
      <Footer />
    </>
  );
};

export default Main;
