const cookieSession = require("cookie-session");
const express = require("express");
const passportSetUp = require("./passport");
const cors = require("cors");
const passport = require("passport");
const authRoute = require("./routes/auth");
const bodyParser = require("body-parser");
require("dotenv").config();

const app = express();
app.use(express.json())

app.use(
  cookieSession({
    name: "session",
    keys: ["jae"],
    maxAge: 24 * 60 * 60 * 100,
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.use(
  cors({
    origin: process.env.CLIENT_URL,
    methods: "GET,PUT,POST,DELETE",
    credentials: true,
  })
);

app.use(bodyParser.json());

app.use("/auth", authRoute);

app.listen("5000", () => {
  console.log("server is running");
});
