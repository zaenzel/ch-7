import React from "react";
import Car from "../images/img_car.png";
import { Box, Grid, Typography } from "@mui/material";

const Hero = () => {
  return (
    <>
      <Grid container>
        <Grid
          item
          md={6}
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Box
            sx={{
              alignItems: "center",
              justifyContent: "center",
              mx: 10,
            }}
          >
            <Typography variant="titleHero" as="h1" mb={2}>
              Sewa & Rental Mobil Terbaik Kawasan (Lokasimu)
            </Typography>
            <Typography variant="body" paragraph maxWidth="90%">
              Selamat Datang di Binar Car Rental. Kami Menyediakan Mobil
              Kualitas Terbaik Dengan Harga Terjangkau. Selalu Siap Melayani
              Kebutuhanmu Untuk Sewa Mobil Selama 24 Jam
            </Typography>
          </Box>
        </Grid>

        <Grid item xs={12} md={6}>
          <Box>
            <img src={Car} alt="car" style={{ width: "100%" }} />
          </Box>
        </Grid>
      </Grid>
    </>
  );
};

export default Hero;
