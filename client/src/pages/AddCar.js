import React from "react";
import { Stack } from "@mui/material";
import SideBar from "../componentAdmin/SideBar";
import FormAddCar from "../componentAdmin/FormAddCar";

const AddCar = () => {
  return (
    <>
      <Stack direction='row' justifyContent='space-between'>
        <SideBar />
        <FormAddCar />
      </Stack>
    </>
  );
};

export default AddCar;
